﻿using System;
using System.Linq;

namespace pidac.learning.codewars.netcore
{
    public class BitCounter
    {
        public static int GetNumberOfBitsEqualsTo1(int number)
        {
            var toBase2 = Convert.ToString(number, 2);
            return toBase2.Count(c => c.Equals('1'));
        }
    }
}
