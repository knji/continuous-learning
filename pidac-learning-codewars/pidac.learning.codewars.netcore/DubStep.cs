﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace pidac.learning.codewars
{
    public class DubStep
    {
        public static string GetOriginalSong(string input)
        {
            var pattern = "(WUB)+";

            var matches = Regex.Matches(input, pattern);
            var sentence = new StringBuilder("");

            if (matches.Count == 0)
            {
                sentence.Append(input);
            }
            else
            {
                for (var i = 0; i < matches.Count; i++)
                {
                    var currentMatch = matches[i];

                    Console.WriteLine(currentMatch);

                    if (i == 0 && currentMatch.Index > 0)
                    {
                        // no matches in front of string
                        sentence.Append(input.Substring(0, currentMatch.Index) + " ");
                    }

                    var wordStartIndex = currentMatch.Index + currentMatch.Length;
                    if (i == matches.Count - 1 && wordStartIndex < input.Length)
                    {
                        // there are some characters after match
                        sentence.Append(input.Substring(wordStartIndex));
                    }

                    var wordLength = 0;

                    if (i < matches.Count - 1)
                    {
                        wordLength = matches[i + 1].Index - wordStartIndex;
                    }

                    sentence.Append(input.Substring(wordStartIndex, wordLength) + " ");

                }
            }

            return sentence.ToString().Trim();
        }
    }
}
