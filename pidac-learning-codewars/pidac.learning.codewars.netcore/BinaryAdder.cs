﻿using System;
namespace pidac.learning.codewars.netcore
{
    public class BinaryAdder
    {
        public string Add(int a, int b)
        {
            var sum = a + b;
            return Convert.ToString(sum, 2);
        }
    }
}
