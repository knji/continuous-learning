﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace pidac.learning.codewars.netcore
{
    public class ParenthesisChecker
    {
        public bool ContainsValidParenthesis(string input)
        {
            if (string.IsNullOrEmpty(input))
                return true;

            if (!input.Contains("(") || !input.Contains(")"))
                return false;

            Stack<char> stack = new Stack<char>();

            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] == '(')
                {
                    stack.Push('(');
                }
                else if (input[i] == ')')
                {
                    if (stack.Count == 0)
                    {
                        return false;
                    }
                    else
                    {
                        stack.Pop();
                    }
                }
                else
                {
                    continue;
                }
            }

            return stack.Count == 0;
        }


        public bool ContainsValidParenthesisGroups(string input)
        {
            var length = input.Length;

            if (length == 1)
                return false;
  
            var closingBracket = ')';
            var openingBracket = '(';

            //var firstChar = input[0];
            //if (firstChar == closingBracket)
            //    return false;

            Stack<char> openBrackets = new Stack<char>();

            //openBrackets.Push(firstChar);

            // symmetrical
            // ()))((()())())
            for (var i = 0; i < length; i++)
            {
                var nextChar = input[i];
                if (nextChar == closingBracket)
                {
                    if (openBrackets.Count == 0)
                    {
                        return false;
                    }

                    openBrackets.Pop();
                    continue;
                }

                if (nextChar == openingBracket)
                {
                    openBrackets.Push(nextChar);
                }
            }

            // return true if all tuples contain opening and closing.
            return openBrackets.Count == 0;
        }
    }
}
