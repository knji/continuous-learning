﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace pidac.learning.codewars.netcore
{
    public class ProductFibonacci
    {
        public static ulong[] ProductFib(ulong prod)
        {
            var fibs = productFib(prod);

            return fibs.ToArray();
        }

        // Thanks to Manoj
        public static ulong[] productFib(ulong prod)
        {
            ulong[] ans = new ulong[3];

            ulong prev1 = 0;
            ulong prev2 = 1;

            while (true)
            {
                ulong next = prev1 + prev2;
                ulong multi = next * prev2;
                if (multi == prod)
                {
                    Console.WriteLine(string.Format("multi == prod: {0}, {1}, {2}", multi, prev2, prev1));
                    ans[0] = prev2;
                    ans[1] = next;
                    ans[2] = 1;

                    return ans;
                }

                if (multi > prod)
                {
                    Console.WriteLine(string.Format("multi > prod: {0}, {1}, {2}", multi, prev2, prev1));
                    ans[0] = prev2;
                    ans[1] = next;
                    ans[2] = 0;

                    return ans;
                }
                
                    Console.WriteLine(string.Format("multi  prod: {0}, {1}, {2}", multi, prev2, prev1));
                    prev1 = prev2;
                    prev2 = next;
                
            }
        }

        /*
        public static ulong GetFibonacciNumbers(int number, List<ulong> accumulator)
        {
            ulong fib = 0;

            if (number == 0)
            {
                fib = 0;
                accumulator.Add(fib);
                return fib;
            }


            if (number == 1)
            {
                fib = 1;
                accumulator.Add(0);
                accumulator.Add(fib);
                return fib;
                   
            }


            fib = GetFibonacciNumbers(number, accumulator) + GetFibonacciNumbers(number - 1, accumulator);
            accumulator.Add(fib);

            return fib;
        }
        */

    }
}
