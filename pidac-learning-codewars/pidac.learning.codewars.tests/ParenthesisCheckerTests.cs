﻿using System;
using NUnit.Framework;
using pidac.learning.codewars.netcore;

namespace pidac.learning.codewars.tests
{
    public class ParenthesisCheckerTests
    {
        [Test]
        [TestCase("()", ExpectedResult = true)]
        [TestCase(")(()))", ExpectedResult = false)]
        [TestCase("(", ExpectedResult = false)]
        [TestCase("(())((()())())", ExpectedResult = true)]
        [TestCase("(()))((()())())", ExpectedResult = false)]
        [TestCase("))))))FFFDD)))))", ExpectedResult = false)]
        [TestCase("()()(((((())(()()(", ExpectedResult = false)]
        [TestCase("(})", ExpectedResult = true)]
        public bool shouldPerformCorrectCheck(string input)
        {
            return new ParenthesisChecker().ContainsValidParenthesisGroups(input); ;
        }
        [Test]
        [TestCase("()", ExpectedResult = true)]
        [TestCase(")(()))", ExpectedResult = false)]
        [TestCase("(", ExpectedResult = false)]
        [TestCase("(())((()())())", ExpectedResult = true)]
        [TestCase("(()))((()())())", ExpectedResult = false)]
        [TestCase("))))))FFFDD)))))", ExpectedResult = false)]
        [TestCase("()()(((((())(()()(", ExpectedResult = false)]
        [TestCase("(})", ExpectedResult = true)]
        public bool shouldPerformCorrectParenthesisCheck(string input)
        {
            return new ParenthesisChecker().ContainsValidParenthesis(input); ;
        }


    }
}
