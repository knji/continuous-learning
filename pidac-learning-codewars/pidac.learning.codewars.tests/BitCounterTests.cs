﻿using System;
using NUnit.Framework;
using pidac.learning.codewars.netcore;

namespace pidac.learning.codewars.tests
{
    public class BitCounterTests
    {
        [Test]
        [TestCase(0, ExpectedResult = 0)]
        [TestCase(1, ExpectedResult = 1)]
        [TestCase(2, ExpectedResult = 1)]
        [TestCase(3, ExpectedResult = 2)]
        [TestCase(4, ExpectedResult = 1)]
        [TestCase(5, ExpectedResult = 2)]
        public int should_return_number_of_bits_equals_to_1(int number)
        {
            return BitCounter.GetNumberOfBitsEqualsTo1(number);
        }
    }
}
