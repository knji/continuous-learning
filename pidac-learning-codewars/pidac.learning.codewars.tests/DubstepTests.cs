using NUnit.Framework;

namespace pidac.learning.codewars.tests
{
    public class DubstepTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        [TestCase("U", ExpectedResult = "U")]
        [TestCase(arg:"ABCWUBDEF", ExpectedResult = "ABC DEF")]
        [TestCase("WUBWUBABCWUB", ExpectedResult = "ABC")]
        [TestCase("WUBWUBAWUBBCWUB", ExpectedResult = "A BC")]
        [TestCase("WUBWUBAWUBBCWUBD", ExpectedResult = "A BC D")]
        [TestCase("UWUB", ExpectedResult = "U")]
        [TestCase("WUBU", ExpectedResult = "U")]
        [TestCase("WUBWUBWUBU", ExpectedResult = "U")]
        [TestCase("WUBUWUBWUB", ExpectedResult = "U")]
        [TestCase("WUUUUUWUB", ExpectedResult = "WUUUUU")]
        [TestCase("WUBWUUUUUWUB", ExpectedResult = "WUUUUU")]
        public string shouldReturn(string input)
        {
            return DubStep.GetOriginalSong(input);
        }
    }
}