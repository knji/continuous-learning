﻿using System;
using NUnit.Framework;
using pidac.learning.codewars.netcore;

namespace pidac.learning.codewars.tests
{
    public class ProductFibonacciTests
    {
        [Test]
        [TestCase(0uL, ExpectedResult = new ulong[] { 1, 1, 0 })]
        [TestCase(1uL, ExpectedResult = new ulong[] { 1, 1, 1 })]
        [TestCase(4895uL, ExpectedResult = new ulong[] { 55, 89, 1 })]
        public ulong[] should_return_fibonacci_sequence(ulong number)
        {
            return ProductFibonacci.ProductFib(number);
        }
    }
}
