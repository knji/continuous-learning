﻿using System;
using NUnit.Framework;
using pidac.learning.codewars.netcore;

namespace pidac.learning.codewars.tests
{
    public class BinaryAdderTests
    {
        [Test]
        [TestCase(1, 0, ExpectedResult = "1")]
        [TestCase(1, 1, ExpectedResult = "10")]
        [TestCase(1, 2, ExpectedResult = "11")]
        public string should_add_to_numbers_and_return_binary(int a, int b)
        {
            var sut = new BinaryAdder();
            return sut.Add(a, b);
            
        }
    }
}
